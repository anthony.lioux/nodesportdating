const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const SportSchema = new Schema({
  design: { type: String },
  users: [{ type: mongoose.Schema.Types.ObjectId, ref: "user" }],
  niveau: [{ type: mongoose.Schema.Types.ObjectId, ref: "pratique" }],
});

const Sport = mongoose.model("Sport", SportSchema);

module.exports = Sport;
