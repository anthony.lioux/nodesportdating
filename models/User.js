const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const UserSchema = new Schema({
  prenom: { type: String, min: 4, max: 25 },
  nom: { type: String, min: 4, max: 25 },
  mail: { type: String, required: true, min: 6, max: 255 },
  depart: { type: String, min: 2, max: 25 },
  password: { type: String, required: true, min: 6, max: 1024 },
  sports: [{ type: mongoose.Schema.Types.ObjectId, ref: "sport" }],
});

const User = mongoose.model("User", UserSchema);

module.exports = User;
