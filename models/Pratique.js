const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const PratiqueSchema = new Schema({
  users: [{ type: mongoose.Schema.Types.ObjectId, ref: "user" }],
  sports: [{ type: mongoose.Schema.Types.ObjectId, ref: "sport" }],
  niveau: { type: String },
});

const Pratique = mongoose.model("Pratique", PratiqueSchema);

module.exports = Pratique;
