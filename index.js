const express = require("express");
const app = express();
const PORT = 5000;
const path = require("path");
const dotenv = require("dotenv");
dotenv.config();
const mongoose = require("mongoose");

mongoose
  .connect(process.env.DB_CONNECT, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => console.log(`[MONGODB is connected !]`))
  .catch((err) => console.log(Error, err.message));

const pratiqueRouter = require("./route/pratiqueRoute");
const sportRouter = require("./route/sportRoute");
const userRouter = require("./route/userRoute");

app.use(express.static("public"));
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "pug");
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.get("/", (req, res) => {
  res.send("Hello World !");
});

app.use("/pratiques", pratiqueRouter);
app.use("/sports", sportRouter);
app.use("/users", userRouter);
app.listen(PORT, () => {
  console.log("Application is working properly !");
});
