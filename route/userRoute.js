const userRouter = require("express").Router();
const User = require("../models/User");
const { registerValidation, loginValidator } = require("../validation");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

userRouter.post("/register", async (req, res) => {
  const { error } = registerValidation(req.body);
  if (error) return res.status(400).json({ error: error.details[0].message });
  // check if email already used
  const emailExist = await User.findOne({ email: req.body.email });
  if (emailExist) return res.status(400).json({ email: "déjà existant" });

  // hash the password

  const salt = await bcrypt.genSalt(10);
  const hashPassword = await bcrypt.hash(req.body.password, salt);

  // création d'un userRouter
  const user = new User({
    name: req.body.name,
    email: req.body.email,
    password: hashPassword,
  });
  try {
    const savedUser = await user.save();
    res.json({ user: user._id });
  } catch (error) {
    res.status(400).json(error);
  }
});
userRouter.post("/login", async (req, res) => {
  // Validation before connect user
  const { error } = loginValidator(req.body);
  if (error) return res.status(400).json({ error: error.details[0].message });
  // Check if email already exist
  const user = await User.findOne({ email: req.body.email });
  if (!user) return res.status(400).json("Email incorrect");
  // CORRECT PASSWORD
  const validPassword = await bcrypt.compare(req.body.password, user.password);
  if (!validPassword) return res.status(400).json("Password incorrect");
  // create & assign token
  const token = jwt.sign({ _id: user._id }, process.env.TOKEN_SECRET);

  res.header("auth-token", token).send(token);
  // res.json({message:"Vous êtes connecté",user}
});

module.exports = userRouter;
